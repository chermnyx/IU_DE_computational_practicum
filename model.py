import math
from typing import Callable, NamedTuple


class Equation(NamedTuple):
    y_prime: Callable[[float, float], float]
    y_exact: Callable[[float, float], float]
    C: Callable[[float, float], float]


equation = Equation(
    y_prime=lambda x, y: 3 * y - x * y ** (1 / 3),

    # from the analytical solution:
    y_exact=lambda x, C: (C * math.exp(2 * x) + 2 * x + 1) ** (3 / 2) / (6 ** (3 / 2)),
    C=lambda x0, y0: (6 * y0 ** (2 / 3) - 2 * x0 - 1) * math.exp(-2 * x0)
)


class Grid(list):
    class Row(NamedTuple):
        x: float
        y: float
        y_exact: float
        lte: float
        gte: float

    def __init__(self, x0: float, y0: float, X: float, N: int):
        super().__init__()
        self.x0 = x0
        self.y0 = y0
        self.X = X
        self.N = N

    @property
    def h(self):
        return (self.X - self.x0) / self.N


class NumericSolver(Grid):
    def __init__(self, equation: Equation, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.equation = equation
        self.C = self.equation.C(self.x0, self.y0)

    def _method_iteration(self, x_prev: float, y_prev: float) -> float:
        """
        :return: y_next
        """
        raise NotImplemented("The numeric method is not implemented")

    def __lte(self, xi: float, xprev: float):
        return abs(self.equation.y_exact(xi, self.C) - self._method_iteration(xprev,
                                                                              self.equation.y_exact(xprev,
                                                                                                    self.C)))

    def __gte(self, xi: float, yi: float):
        return abs(self.equation.y_exact(xi, self.C) - yi)

    def __add_row(self, x: float, y: float):
        self.append(Grid.Row(x=x, y=y,
                             y_exact=self.equation.y_exact(x, self.C),
                             lte=self.__lte(xi=x, xprev=self[-1].x),
                             gte=self.__gte(xi=x, yi=y)))

    def solve(self):
        self.append(Grid.Row(x=self.x0, y=self.y0,
                             y_exact=equation.y_exact(self.x0, self.C),
                             lte=0., gte=self.__gte(xi=self.x0, yi=self.y0)))

        for i in range(1, self.N + 1):
            self.__add_row(self[-1].x + self.h, self._method_iteration(self[-1].x, self[-1].y))


class EulerSolver(NumericSolver):
    def _method_iteration(self, x_prev: float, y_prev: float) -> float:
        return y_prev + self.h * self.equation.y_prime(x_prev, y_prev)


class ImprovedEulerSolver(NumericSolver):
    def _method_iteration(self, x_prev: float, y_prev: float) -> float:
        k1 = self.equation.y_prime(x_prev, y_prev)
        k2 = self.equation.y_prime(x_prev + self.h, y_prev + self.h * k1)
        return y_prev + self.h / 2 * (k1 + k2)


class RungeKuttaSolver(NumericSolver):
    def _method_iteration(self, x_prev: float, y_prev: float) -> float:
        k1 = self.equation.y_prime(x_prev, y_prev)
        k2 = self.equation.y_prime(x_prev + self.h / 2, y_prev + self.h / 2 * k1)
        k3 = self.equation.y_prime(x_prev + self.h / 2, y_prev + self.h / 2 * k2)
        k4 = self.equation.y_prime(x_prev + self.h, y_prev + self.h * k3)

        return y_prev + self.h / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
