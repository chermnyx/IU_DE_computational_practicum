import random
from typing import Dict

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure

from model import Equation, equation, NumericSolver, EulerSolver, ImprovedEulerSolver, RungeKuttaSolver

DOUBLE_STEP = 0.01
MAX_ABS = 2 ** 31 - 1
random.seed(123456789)


class Configuration(QWidget):
    def __init__(self, equation: Equation, methods: Dict[str, NumericSolver]):
        super().__init__()

        self.equation = equation
        self.methods = {name: {'on': QCheckBox(name), 'solver': solver, 'color': f'#{random.randint(0, 0xFFFFFF):06x}'}
                        for name, solver in methods.items()}
        self.exact_color = f'#{random.randint(0, 0xFFFFFF):06x}'
        for x in self.methods.values():
            x['on'].toggle()

        self.solution_params = QGroupBox('Solution Parameters')
        self.x0 = QDoubleSpinBox()
        self.x0.setValue(1)
        self.x0.setSingleStep(DOUBLE_STEP)
        self.x0.setMinimum(-MAX_ABS)
        self.x0.setMaximum(MAX_ABS)
        self.X = QDoubleSpinBox()
        self.X.setValue(6)
        self.X.setSingleStep(DOUBLE_STEP)
        self.X.setMinimum(-MAX_ABS)
        self.X.setMaximum(MAX_ABS)
        self.x0.valueChanged.connect(lambda e: self.X.setMinimum(e))
        self.X.valueChanged.connect(lambda e: self.x0.setMaximum(e))
        self.y0 = QDoubleSpinBox()
        self.y0.setValue(2)
        self.y0.setSingleStep(DOUBLE_STEP)
        self.y0.setMinimum(-MAX_ABS)
        self.y0.setMaximum(MAX_ABS)
        self.N = QSpinBox()
        self.N.setSingleStep(1)
        self.N.setMinimum(2)
        self.N.setMaximum(100_000)
        self.N.setValue(100)
        self.solution_params_layout = QFormLayout()
        for x in ('x0', 'X', 'y0', 'N'):
            self.solution_params_layout.addRow(QLabel(x), getattr(self, x))
        self.solution_params.setLayout(self.solution_params_layout)

        self.methods_widget = QGroupBox('Methods')
        self.methods_layout = QFormLayout()
        for method, cls in self.methods.items():
            self.methods_layout.addRow(cls['on'])
        self.methods_widget.setLayout(self.methods_layout)

        self.error_analyse = QGroupBox('Grid size')
        self.error_analyse_layout = QFormLayout()
        self.N_min = QSpinBox()
        self.N_max = QSpinBox()
        self.N_min.setMinimum(2)
        self.N_min.setMaximum(300)
        self.N_max.setMinimum(2)
        self.N_max.setMaximum(300)
        self.N_max.setValue(100)
        self.N_min.valueChanged.connect(lambda e: self.N_max.setMinimum(e))
        self.N_max.valueChanged.connect(lambda e: self.N_min.setMaximum(e))
        self.error_analyse_layout.addRow(QLabel('N_min'), self.N_min)
        self.error_analyse_layout.addRow(QLabel('N_max'), self.N_max)
        self.error_analyse.setLayout(self.error_analyse_layout)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.solution_params)
        self.layout.addWidget(self.methods_widget)
        self.layout.addWidget(self.error_analyse)

        self.go = QPushButton('PLOT')
        self.layout.addWidget(self.go)

        self.setLayout(self.layout)


class MatplotlibWidget(FigureCanvasQTAgg):
    def __init__(self, **kwargs):
        fig = Figure(**kwargs)
        self.ax = fig.add_subplot(111)

        super().__init__(fig)

    def plot(self):
        self.ax.clear()

    def post_plot(self):
        self.ax.grid(True, which='both')
        # self.ax.axis('equal')
        self.ax.legend(loc='upper right')
        self.draw()


class SolutionTab(QTabWidget):
    def __init__(self, cfg: Configuration):
        super().__init__()
        self.exact_sol = None
        self.solvers: Dict[str, NumericSolver] = {}

        self.cfg = cfg
        self.graph1 = self.ValueGraph(parent=self)
        self.graph2 = self.LTEGraph(parent=self)
        self.graph3 = self.GTEGraph(parent=self)

        self.addTab(self.graph1, 'Solution')
        self.addTab(self.graph2, 'LTE')
        self.addTab(self.graph3, 'GTE')

        self.calculate()

    def calculate(self):
        self.exact_sol = None

        for method, props in self.cfg.methods.items():
            if not props['on'].checkState():
                continue
            solver: NumericSolver = props['solver'](self.cfg.equation,
                                                    x0=self.cfg.x0.value(),
                                                    y0=self.cfg.y0.value(),
                                                    X=self.cfg.X.value(),
                                                    N=self.cfg.N.value())
            solver.solve()
            if self.exact_sol is None:
                self.exact_sol = solver

            self.solvers[method] = solver

    class ValueGraph(MatplotlibWidget):
        def __init__(self, parent):
            super().__init__()
            self.parent = parent
            self.cfg = parent.cfg

        def plot(self):
            super().plot()

            for method, solver in self.parent.solvers.items():
                if not self.cfg.methods[method]['on'].checkState():
                    continue
                self.ax.plot([r.x for r in solver], [r.y for r in solver],
                             label=method,
                             color=self.cfg.methods[method]['color'])

            self.ax.plot([r.x for r in self.parent.exact_sol], [r.y_exact for r in self.parent.exact_sol],
                         label='EXACT',
                         color=self.cfg.exact_color)

            self.post_plot()

    class LTEGraph(MatplotlibWidget):
        def __init__(self, parent):
            super().__init__()
            self.parent = parent
            self.cfg = parent.cfg

        def plot(self):
            super().plot()

            for method, solver in self.parent.solvers.items():
                if not self.cfg.methods[method]['on'].checkState():
                    continue
                self.ax.plot([r.x for r in solver], [r.lte for r in solver],
                             label=method,
                             color=self.cfg.methods[method]['color'])

            self.post_plot()

    class GTEGraph(MatplotlibWidget):
        def __init__(self, parent):
            super().__init__()
            self.parent = parent
            self.cfg = parent.cfg

        def plot(self):
            super().plot()

            for method, solver in self.parent.solvers.items():
                if not self.cfg.methods[method]['on'].checkState():
                    continue
                self.ax.plot([r.x for r in solver], [r.gte for r in solver],
                             label=method,
                             color=self.cfg.methods[method]['color'])

            self.post_plot()

    def plot(self):
        self.calculate()
        self.graph1.plot()
        self.graph2.plot()
        self.graph3.plot()


class TotalErrorAnalyseTab(MatplotlibWidget):
    def __init__(self, cfg: Configuration):
        super().__init__()
        self.cfg = cfg

    def plot(self):
        super().plot()

        points = {method: {'n': [], 'gte': []} for method in self.cfg.methods}
        for n in range(self.cfg.N_min.value(), self.cfg.N_max.value() + 1):
            for method, props in self.cfg.methods.items():
                if not props['on'].checkState():
                    continue
                solver: NumericSolver = props['solver'](self.cfg.equation,
                                                        x0=self.cfg.x0.value(),
                                                        y0=self.cfg.y0.value(),
                                                        X=self.cfg.X.value(),
                                                        N=n)
                solver.solve()
                points[method]['n'].append(n)
                points[method]['gte'].append(max(x.gte for x in solver))

        for method in self.cfg.methods:
            self.ax.plot(points[method]['n'], points[method]['gte'],
                         label=method,
                         color=self.cfg.methods[method]['color'])

        super().post_plot()


class Main(QWidget):
    def __init__(self):
        super().__init__()

        self.layout = QHBoxLayout()
        self.config = Configuration(equation, {x.__name__: x
                                               for x in (EulerSolver, ImprovedEulerSolver, RungeKuttaSolver)})

        self.tab1 = SolutionTab(self.config)
        self.tab2 = TotalErrorAnalyseTab(self.config)
        self.tabs = QTabWidget()
        self.tabs.setMinimumSize(300, 300)
        self.tabs.addTab(self.tab1, "Solution")
        self.tabs.addTab(self.tab2, "GTE(N) Analyse")

        self.layout.addWidget(self.tabs)
        self.layout.addWidget(self.config)

        self.config.go.clicked.connect(self._redraw)
        self.err = QErrorMessage()

        self.setLayout(self.layout)

    def _redraw(self, e):
        try:
            self.tab1.plot()
            self.tab2.plot()
        except (OverflowError, ArithmeticError) as e:
            self.err.showMessage(f'Mathematical error: {e}')


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.widget = Main()
        self.setCentralWidget(self.widget)

        self.setWindowTitle('DE Computational practicum')
        self.setMinimumSize(self.widget.layout.sizeHint())
        self.show()
