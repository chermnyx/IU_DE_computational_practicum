# DE Computational Practicum

Dmitrii Chermnykh (d.chermnykh@innopolis.university), BS19-06

Full source code: <https://gitlab.com/chermnyx/IU_DE_computational_practicum>

## The exact solution

$\begin{cases}
y' = f(x, y) \\
y(x_0) = y_0 \\
x \in [x_0; X]
\end{cases}$ where $\begin{cases}
f(x, y) = 3y - xy^{1/3} \\
x_0 = 1 \\
y_0 = 2 \\
X = 6
\end{cases}$

$y' = 3y - xy^{1/3}$

y=0 is a trivial solution

Let $z = z(x)$ be a function such that $z'-3z=0$

$$
dz = 3z dx \\
{dz \over z} = 3 dx \\
\ln |z| = 3x + C_0 \\
z = C_1 e^{3x}
$$

Let $u = u(x)$ such that $y = uz$. Then $y' = u'z + z'u$

$$
u'z + u(z'-3z) + x(uz)^{1/3} = 0 \\
u'C_1e^{3x} + xu^{1/3}C_1^{1/3}e^x = 0 \\
u'C_1e^{2x} + xu^{1/3}C_1^{1/3} = 0\\
C_1u^{-1/3}du +xe^{-2x}C_1^{1/3}dx = 0 \\
$$

After integration:

$$
{3 \over 2}C_1u^{2/3} - {1 \over 4}C_1^{1/3}e^{-2x}(2x + 1) = C_2 \\
u = {y \over z} = {y \over C_1e^{3x}} \text{ where } C_1 \neq 0 \\
{3 \over 2}(C_1 u)^{2/3} - {1 \over 4}e^{-2x}(2x+1) = C_3 \\
{3 \over 2}y^{2/3} - {1 \over 4}(2x+1) = C_3e^{2x} \\
y^{2/3} = C_4e^{2x}+{1 \over 6}(2x+1)
$$

$y={ (\bold{C}e^{2x} + 2x +1)^{3/2} \over 6^{3/2}}$

Let us find C:

$$
6y_0^{2/3}=Ce^{2x_0}+2x_0+1 \\
C = (6y_0^{2/3}-2x_0-1)e^{-2x_0}
$$

Exact solution:
$y={ (\bold{C}e^{2x} + 2x +1)^{3/2} \over 6^{3/2}}$ where $\bold{C} = (6y_0^{2/3}-2x_0-1)e^{-2x_0}$

For $x_0=1$ and $y_0=2$:

$$
C = {6 \cdot 2^{2/3} - 3 \over e^2} \\
y = {\left({6 \cdot 2^{2/3} - 3 \over e^2}e^{2x}+2x+1\right)^{3/2} \over 6^{3/2}}
$$

Points of discontinuity: All the points thas satisfy $Ce^{2x}+2x+1 < 0$ (because of power $3/2$)

## Graphs $y(x), LTE(x), GTE(x)$ and $GTE(N)$

- $y(x)$

  - for the given values
    ![graph](assets/Screenshot_20201026_175958.png)

  - for changed values
    ![graph](assets/Screenshot_20201026_180232.png)

- $LTE(x)$ for the given values
  ![graph](assets/Screenshot_20201026_180008.png)

- $GTE(x)$ for the given values
  ![graph](assets/Screenshot_20201026_180015.png)

- $GTE(N)$

  The more grid cells we have, the less approximation errors we get. All methods converge with different speed. The fastest one is Runge-Kutta method.

  - for given values
    ![graph](assets/Screenshot_20201026_180025.png)
  - for changed values
    ![graph](assets/Screenshot_20201026_180301.png)

As you can see from the graphs, the most accurate method is Runge-Kutta method ans the less accurate method is Euler method.

## UML Diagram and some parts of the source code

![UML](./assets/ComputationalPracticum.png)

### The equation with exact solution

```python
equation = Equation(
    y_prime=lambda x, y: 3 * y - x * y ** (1 / 3),

    # from the analytical solution:
    y_exact=lambda x, C: (C * math.exp(2 * x) + 2 * x + 1) ** (3 / 2) / (6 ** (3 / 2)),
    C=lambda x0, y0: (6 * y0 ** (2 / 3) - 2 * x0 - 1) * math.exp(-2 * x0)
)
```

### Common code for all solvers

```python
class Grid(list):
    class Row(NamedTuple):
        x: float
        y: float
        y_exact: float
        lte: float
        gte: float

    def __init__(self, x0: float, y0: float, X: float, N: int):
        super().__init__()
        self.x0 = x0
        self.y0 = y0
        self.X = X
        self.N = N

    @property
    def h(self):
        return (self.X - self.x0) / self.N


class NumericSolver(Grid):
    def __init__(self, equation: Equation, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.equation = equation
        self.C = self.equation.C(self.x0, self.y0)

    def _method_iteration(self, x_prev: float, y_prev: float) -> float:
        """
        :return: y_next
        """
        raise NotImplemented("The numeric method is not implemented")

    def __lte(self, xi: float, xprev: float):
        return abs(self.equation.y_exact(xi, self.C) - self._method_iteration(xprev,
                                                                              self.equation.y_exact(xprev,
                                                                                                    self.C)))

    def __gte(self, xi: float, yi: float):
        return abs(self.equation.y_exact(xi, self.C) - yi)

    def __add_row(self, x: float, y: float):
        self.append(Grid.Row(x=x, y=y,
                             y_exact=self.equation.y_exact(x, self.C),
                             lte=self.__lte(xi=x, xprev=self[-1].x),
                             gte=self.__gte(xi=x, yi=y)))

    def solve(self):
        self.append(Grid.Row(x=self.x0, y=self.y0,
                             y_exact=equation.y_exact(self.x0, self.C),
                             lte=0., gte=self.__gte(xi=self.x0, yi=self.y0)))

        for i in range(1, self.N + 1):
            self.__add_row(self[-1].x + self.h, self._method_iteration(self[-1].x, self[-1].y))
```

### Euler Method

```python
class EulerSolver(NumericSolver):
    def _method_iteration(self, x_prev: float, y_prev: float) -> float:
        return y_prev + self.h * self.equation.y_prime(x_prev, y_prev)
```

### Improved Euler Method

```python
class ImprovedEulerSolver(NumericSolver):
    def _method_iteration(self, x_prev: float, y_prev: float) -> float:
        k1 = self.equation.y_prime(x_prev, y_prev)
        k2 = self.equation.y_prime(x_prev + self.h, y_prev + self.h * k1)
        return y_prev + self.h / 2 * (k1 + k2)
```

### Runge-Kutta Method

```python
class RungeKuttaSolver(NumericSolver):
    def _method_iteration(self, x_prev: float, y_prev: float) -> float:
        k1 = self.equation.y_prime(x_prev, y_prev)
        k2 = self.equation.y_prime(x_prev + self.h / 2, y_prev + self.h / 2 * k1)
        k3 = self.equation.y_prime(x_prev + self.h / 2, y_prev + self.h / 2 * k2)
        k4 = self.equation.y_prime(x_prev + self.h, y_prev + self.h * k3)

        return y_prev + self.h / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
```

### An example of graph plotter

```python
class ValueGraph(MatplotlibWidget):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.cfg = parent.cfg

    def plot(self):
        super().plot()

        for method, solver in self.parent.solvers.items():
            if not self.cfg.methods[method]['on'].checkState():
                continue
            self.ax.plot([r.x for r in solver], [r.y for r in solver],
                            label=method,
                            color=self.cfg.methods[method]['color'])

        self.ax.plot([r.x for r in self.parent.exact_sol], [r.y_exact for r in self.parent.exact_sol],
                        label='EXACT',
                        color=self.cfg.exact_color)

        self.post_plot()
```
